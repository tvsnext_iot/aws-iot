#!/usr/bin/python

import paho.mqtt.client as mqtt
import os
import re
import json
import socket
import ssl
from time import sleep
import time
import csv
import boto
import boto3
from btle import UUID, Peripheral, DefaultDelegate
import struct
import math
import sys
import argparse
from boto.s3.key import Key
s3_client = boto3.client('s3')
client = boto3.client('iot')
iam = boto3.client('iam')
l = boto3.client('lambda')


tag=''
arg = ''
thing_name=''
connflag = False
count=0
flag=0
timestr1=0
folder=''
date=''




#============================================ Connect To AWS IoT ====================================================================



def on_connect(client, userdata, flags, rc):
	global thing_name
	if rc==0:
	 	mqtt_client.subscribe('$aws/things/'+ thing_name + '/shadow/update/delta',qos=0)
	


def on_message(client, userdata, msg):
    print("Message received : "+msg.topic+" | Qos: "+str(msg.qos)+" |   Date Received: "+str(msg.payload))


mqtt_client = mqtt.Client(client_id=thing_name,clean_session=True)
awshost = "AE2J99KNLEONC.iot.us-east-1.amazonaws.com"
awsport = 8883
clientId = thing_name



#==================================== Store the sensortag data files into local directory ===================================



def write_to_file(data):
	
	global flag
	timestr =  time.strftime("%d/%m/%Y-%H:%M:%S")
	fi=dict()	
	di={'Temperature':data['temp'],'Humidity':data['hum'],'Barometer':data['baro'],'Accelerometer':data['acc1'],'Light':data['lgh']}
	val2=data['acc2']
	val3=data['acc3']
	c=1
	
	
	for key,value in di.items():
		with open(os.path.join('/home/blt0009/AWS_IoT/sensor',data['sensor'] + '_' + key + '_' + data['timestr1'] +  '.csv'), 'a') as csv_file:		
			fi['fil' + str(c)] = data['sensor'] + '_' + key + '_' + data['timestr1'] + '.csv'
			try:	
				fieldnames = ['MAC_ID','SENSOR','VALUE','TIME']
				fieldnames1 = ['MAC_ID','SENSOR','VALUE1','VALUE2','VALUE3','TIME']
				writer = csv.DictWriter(csv_file, delimiter=',', lineterminator='\n',fieldnames=fieldnames)
				writer1 = csv.DictWriter(csv_file, delimiter=',', lineterminator='\n',fieldnames=fieldnames1)
				if flag==0:
					if (key == 'Accelerometer'):
						writer1.writeheader()
					else:
						writer.writeheader()

				if (key == 'Accelerometer'):
						
						writer1.writerow({'MAC_ID':data['sensor'],'SENSOR':key,'VALUE1':value,'VALUE2':val2,'VALUE3':val3,'TIME':timestr})
				else:
						writer.writerow({'MAC_ID':data['sensor'],'SENSOR':key,'VALUE':value,'TIME':timestr})
			finally:
				csv_file.close()
		c=c+1
	flag=flag+1
	return fi
		

		


#====================================== Upload to S3 bucket ==============================================



def upload_to_s3(filename):
	global count
	global timestr1
	global flag
	global folder
	global Bucket
	global date
	upload_count=0
	date = time.strftime("%d.%m.%Y")
	subfolder = folder + date + '/' 
	s3_client.put_object(Bucket='client.bucket',Body='',Key=subfolder)
	for key in filename:
		acc = 'Accelerometer'
		baro = 'Barometer'
		hum = 'Humidity'
		temp= 'Temperature'
		lig ='Light'
		if(acc in filename[key]):
			acce_folder = subfolder + acc + '/'
			s3_client.put_object(Bucket='client.bucket',Body='',Key=acce_folder)
			s3_client.upload_file(filename[key],'client.bucket',acce_folder+filename[key])
		elif(baro in filename[key]):
			baro_folder = subfolder + baro + '/'
			s3_client.put_object(Bucket='client.bucket',Body='',Key=baro_folder)
			s3_client.upload_file(filename[key],'client.bucket',baro_folder+filename[key])
		elif(hum in filename[key]):
			hum_folder = subfolder + hum + '/'
			s3_client.put_object(Bucket='client.bucket',Body='',Key=hum_folder)
			s3_client.upload_file(filename[key],'client.bucket',hum_folder+filename[key])
		elif(temp in filename[key]):
			temp_folder = subfolder + temp + '/'
			s3_client.put_object(Bucket='client.bucket',Body='',Key=temp_folder)
			s3_client.upload_file(filename[key],'client.bucket',temp_folder+filename[key])
			
		else:
			light_folder= subfolder + lig + '/'
			s3_client.put_object(Bucket='client.bucket',Body='',Key=light_folder)
			s3_client.upload_file(filename[key],'client.bucket',light_folder+filename[key])
			
						
		upload_count=upload_count+1
	if (upload_count==5):
		print("==============================================================================")
		print("CSV Files are successfully uploaded to S3")
		os.system('rm -r /home/blt0009/AWS_IoT/sensor/*.csv')
		print("Previous csv file was deleted")
		print("New csv files are created")
		count=0
		flag=0
		timestr1 = time.strftime("%d.%m.%Y_%H:%M")
		print("==============================================================================\n")
	else:
		print("Upload fail")
	


#======================================== FROM THIS SENSORTAG =============================================



def _TI_UUID(val):
    return UUID("%08X-0451-4000-b000-000000000000" % (0xF0000000+val))

# Sensortag versions
AUTODETECT = "-"
SENSORTAG_V1 = "v1"
SENSORTAG_2650 = "CC2650"



class SensorBase:
    # Derived classes should set: svcUUID, ctrlUUID, dataUUID
    sensorOn  = struct.pack("B", 0x01)
    sensorOff = struct.pack("B", 0x00)

    def __init__(self, periph):
        self.periph = periph
        self.service = None
        self.ctrl = None
        self.data = None

    def enable(self):
        if self.service is None:
            self.service = self.periph.getServiceByUUID(self.svcUUID)
        if self.ctrl is None:
            self.ctrl = self.service.getCharacteristics(self.ctrlUUID) [0]
        if self.data is None:
            self.data = self.service.getCharacteristics(self.dataUUID) [0]
        if self.sensorOn is not None:
            self.ctrl.write(self.sensorOn,withResponse=True)

    def read(self):
        return self.data.read()

    def disable(self):
        if self.ctrl is not None:
            self.ctrl.write(self.sensorOff)

    # Derived class should implement _formatData()



def calcPoly(coeffs, x):
    return coeffs[0] + (coeffs[1]*x) + (coeffs[2]*x*x)




class IRTemperatureSensor(SensorBase):
    svcUUID  = _TI_UUID(0xAA00)
    dataUUID = _TI_UUID(0xAA01)
    ctrlUUID = _TI_UUID(0xAA02)

    zeroC = 273.15 # Kelvin
    tRef  = 298.15
    Apoly = [1.0,      1.75e-3, -1.678e-5]
    Bpoly = [-2.94e-5, -5.7e-7,  4.63e-9]
    Cpoly = [0.0,      1.0,      13.4]

    def __init__(self, periph):
        SensorBase.__init__(self, periph)
        self.S0 = 6.4e-14

    def read(self):
        '''Returns (ambient_temp, target_temp) in degC'''

        # See http://processors.wiki.ti.com/index.php/SensorTag_User_Guide#IR_Temperature_Sensor
        (rawVobj, rawTamb) = struct.unpack('<hh', self.data.read())
        tAmb = rawTamb / 128.0
        Vobj = 1.5625e-7 * rawVobj

        tDie = tAmb + self.zeroC
        S   = self.S0 * calcPoly(self.Apoly, tDie-self.tRef)
        Vos = calcPoly(self.Bpoly, tDie-self.tRef)
        fObj = calcPoly(self.Cpoly, Vobj-Vos)

        tObj = math.pow( math.pow(tDie,4.0) + (fObj/S), 0.25 )
        return (tAmb, tObj - self.zeroC)



class IRTemperatureSensorTMP007(SensorBase):
    svcUUID  = _TI_UUID(0xAA00)
    dataUUID = _TI_UUID(0xAA01)
    ctrlUUID = _TI_UUID(0xAA02)

    SCALE_LSB = 0.03125;
 
    def __init__(self, periph):
        SensorBase.__init__(self, periph)

    def read(self):
        '''Returns (ambient_temp, target_temp) in degC'''
        # http://processors.wiki.ti.com/index.php/CC2650_SensorTag_User's_Guide?keyMatch=CC2650&tisearch=Search-EN
        (rawTobj, rawTamb) = struct.unpack('<hh', self.data.read())
        tObj = (rawTobj >> 2) * self.SCALE_LSB;
        tAmb = (rawTamb >> 2) * self.SCALE_LSB;
        return (tAmb, tObj)



class AccelerometerSensor(SensorBase):
    svcUUID  = _TI_UUID(0xAA10)
    dataUUID = _TI_UUID(0xAA11)
    ctrlUUID = _TI_UUID(0xAA12)

    def __init__(self, periph):
        SensorBase.__init__(self, periph)

    def read(self):
        '''Returns (x_accel, y_accel, z_accel) in units of g'''
        x_y_z = struct.unpack('bbb', self.data.read())
        return tuple([ (val/64.0) for val in x_y_z ])



class MovementSensorMPU9250(SensorBase):
    svcUUID  = _TI_UUID(0xAA80)
    dataUUID = _TI_UUID(0xAA81)
    ctrlUUID = _TI_UUID(0xAA82)
    sensorOn = None
    GYRO_XYZ =  7
    ACCEL_XYZ = 7 << 3
    MAG_XYZ = 1 << 6
    ACCEL_RANGE_2G  = 0 << 8
    ACCEL_RANGE_4G  = 1 << 8
    ACCEL_RANGE_8G  = 2 << 8
    ACCEL_RANGE_16G = 3 << 8

    def __init__(self, periph):
        SensorBase.__init__(self, periph)
        self.ctrlBits = 0

    def enable(self, bits):
        SensorBase.enable(self)
        self.ctrlBits |= bits
        self.ctrl.write( struct.pack("<H", self.ctrlBits) )

    def disable(self, bits):
        self.ctrlBits &= ~bits
        self.ctrl.write( struct.pack("<H", self.ctrlBits) )

    def rawRead(self):
        dval = self.data.read()
        return struct.unpack("<hhhhhhhhh", dval)



class AccelerometerSensorMPU9250:
    def __init__(self, sensor_):
        self.sensor = sensor_
        self.bits = self.sensor.ACCEL_XYZ | self.sensor.ACCEL_RANGE_4G
        self.scale = 8.0/32768.0 # TODO: why not 4.0, as documented?

    def enable(self):
        self.sensor.enable(self.bits)

    def disable(self):
        self.sensor.disable(self.bits)

    def read(self):
        '''Returns (x_accel, y_accel, z_accel) in units of g'''
        rawVals = self.sensor.rawRead()[3:6]
        return tuple([ v*self.scale for v in rawVals ])



class HumiditySensor(SensorBase):
    svcUUID  = _TI_UUID(0xAA20)
    dataUUID = _TI_UUID(0xAA21)
    ctrlUUID = _TI_UUID(0xAA22)

    def __init__(self, periph):
        SensorBase.__init__(self, periph)

    def read(self):
        '''Returns (ambient_temp, rel_humidity)'''
        (rawT, rawH) = struct.unpack('<HH', self.data.read())
        temp = -46.85 + 175.72 * (rawT / 65536.0)
        RH = -6.0 + 125.0 * ((rawH & 0xFFFC)/65536.0)
        return (temp, RH)



class HumiditySensorHDC1000(SensorBase):
    svcUUID  = _TI_UUID(0xAA20)
    dataUUID = _TI_UUID(0xAA21)
    ctrlUUID = _TI_UUID(0xAA22)

    def __init__(self, periph):
        SensorBase.__init__(self, periph)

    def read(self):
        '''Returns (ambient_temp, rel_humidity)'''
        (rawT, rawH) = struct.unpack('<HH', self.data.read())
        temp = -40.0 + 165.0 * (rawT / 65536.0)
        RH = 100.0 * (rawH/65536.0)
        return (temp, RH)



class MagnetometerSensor(SensorBase):
    svcUUID  = _TI_UUID(0xAA30)
    dataUUID = _TI_UUID(0xAA31)
    ctrlUUID = _TI_UUID(0xAA32)

    def __init__(self, periph):
        SensorBase.__init__(self, periph)

    def read(self):
        '''Returns (x, y, z) in uT units'''
        x_y_z = struct.unpack('<hhh', self.data.read())
        return tuple([ 1000.0 * (v/32768.0) for v in x_y_z ])
        # Revisit - some absolute calibration is needed



class MagnetometerSensorMPU9250:
    def __init__(self, sensor_):
        self.sensor = sensor_
        self.scale = 4912.0 / 32760
        # Reference: MPU-9250 register map v1.4

    def enable(self):
        self.sensor.enable(self.sensor.MAG_XYZ)

    def disable(self):
        self.sensor.disable(self.sensor.MAG_XYZ)

    def read(self):
        '''Returns (x_mag, y_mag, z_mag) in units of uT'''
        rawVals = self.sensor.rawRead()[6:9]
        return tuple([ v*self.scale for v in rawVals ])



class BarometerSensor(SensorBase):
    svcUUID  = _TI_UUID(0xAA40)
    dataUUID = _TI_UUID(0xAA41)
    ctrlUUID = _TI_UUID(0xAA42)
    calUUID  = _TI_UUID(0xAA43)
    sensorOn = None

    def __init__(self, periph):
       SensorBase.__init__(self, periph)

    def enable(self):
        SensorBase.enable(self)
        self.calChr = self.service.getCharacteristics(self.calUUID) [0]

        # Read calibration data
        self.ctrl.write( struct.pack("B", 0x02), True )
        (c1,c2,c3,c4,c5,c6,c7,c8) = struct.unpack("<HHHHhhhh", self.calChr.read())
        self.c1_s = c1/float(1 << 24)
        self.c2_s = c2/float(1 << 10)
        self.sensPoly = [ c3/1.0, c4/float(1 << 17), c5/float(1<<34) ]
        self.offsPoly = [ c6*float(1<<14), c7/8.0, c8/float(1<<19) ]
        self.ctrl.write( struct.pack("B", 0x01), True )


    def read(self):
        '''Returns (ambient_temp, pressure_millibars)'''
        (rawT, rawP) = struct.unpack('<hH', self.data.read())
        temp = (self.c1_s * rawT) + self.c2_s
        sens = calcPoly( self.sensPoly, float(rawT) )
        offs = calcPoly( self.offsPoly, float(rawT) )
        pres = (sens * rawP + offs) / (100.0 * float(1<<14))
        return (temp,pres)



class BarometerSensorBMP280(SensorBase):
    svcUUID  = _TI_UUID(0xAA40)
    dataUUID = _TI_UUID(0xAA41)
    ctrlUUID = _TI_UUID(0xAA42)

    def __init__(self, periph):
        SensorBase.__init__(self, periph)

    def read(self):
        (tL,tM,tH,pL,pM,pH) = struct.unpack('<BBBBBB', self.data.read())
        temp = (tH*65536 + tM*256 + tL) / 100.0
        press = (pH*65536 + pM*256 + pL) / 100.0
        return (temp, press)



class GyroscopeSensor(SensorBase):
    svcUUID  = _TI_UUID(0xAA50)
    dataUUID = _TI_UUID(0xAA51)
    ctrlUUID = _TI_UUID(0xAA52)
    sensorOn = struct.pack("B",0x07)

    def __init__(self, periph):
       SensorBase.__init__(self, periph)

    def read(self):
        '''Returns (x,y,z) rate in deg/sec'''
        x_y_z = struct.unpack('<hhh', self.data.read())
        return tuple([ 250.0 * (v/32768.0) for v in x_y_z ])




class GyroscopeSensorMPU9250:
    def __init__(self, sensor_):
        self.sensor = sensor_
        self.scale = 500.0/65536.0

    def enable(self):
        self.sensor.enable(self.sensor.GYRO_XYZ)

    def disable(self):
        self.sensor.disable(self.sensor.GYRO_XYZ)

    def read(self):
        '''Returns (x_gyro, y_gyro, z_gyro) in units of degrees/sec'''
        rawVals = self.sensor.rawRead()[0:3]
        return tuple([ v*self.scale for v in rawVals ])




class KeypressSensor(SensorBase):
    svcUUID = UUID(0xFFE0)
    dataUUID = UUID(0xFFE1)
    ctrlUUID = None
    sensorOn = None

    def __init__(self, periph):
        SensorBase.__init__(self, periph)
 
    def enable(self):
        SensorBase.enable(self)
        # NB handle value changed between v1.4 and v1.5 firmware,
        # but is not directly discoverable by UUID. This seems to work.
        self.periph.writeCharacteristic(self.data.handle+2, struct.pack('<bb', 0x01, 0x00), True)

    def disable(self):
        self.periph.writeCharacteristic(self.data.handle+2, struct.pack('<bb', 0x00, 0x00), True)



class OpticalSensorOPT3001(SensorBase):
    svcUUID  = _TI_UUID(0xAA70)
    dataUUID = _TI_UUID(0xAA71)
    ctrlUUID = _TI_UUID(0xAA72)

    def __init__(self, periph):
       SensorBase.__init__(self, periph)

    def read(self):
        '''Returns value in lux'''
        raw = struct.unpack('<h', self.data.read()) [0]
        m = raw & 0xFFF;
        e = (raw & 0xF000) >> 12;
        return 0.01 * (m << e)



class SensorTag(Peripheral):
    def __init__(self,addr,version=AUTODETECT):
        Peripheral.__init__(self,addr)
        if version==AUTODETECT:
            svcs = self.discoverServices()
            if _TI_UUID(0xAA70) in svcs:
                version = SENSORTAG_2650
            else:
                version = SENSORTAG_V1

        if version==SENSORTAG_V1:
            self.IRtemperature = IRTemperatureSensor(self)
            self.accelerometer = AccelerometerSensor(self)
            self.humidity = HumiditySensor(self)
            self.magnetometer = MagnetometerSensor(self)
            self.barometer = BarometerSensor(self)
            self.gyroscope = GyroscopeSensor(self)
            self.keypress = KeypressSensor(self)
            self.lightmeter = None
        elif version==SENSORTAG_2650:
            self._mpu9250 = MovementSensorMPU9250(self)
            self.IRtemperature = IRTemperatureSensorTMP007(self)
            self.accelerometer = AccelerometerSensorMPU9250(self._mpu9250)
            self.humidity = HumiditySensorHDC1000(self)
            self.magnetometer = MagnetometerSensorMPU9250(self._mpu9250)
            self.barometer = BarometerSensorBMP280(self)
            self.gyroscope = GyroscopeSensorMPU9250(self._mpu9250)
            self.keypress = KeypressSensor(self)
            self.lightmeter = OpticalSensorOPT3001(self)


class KeypressDelegate(DefaultDelegate):
    BUTTON_L = 0x02
    BUTTON_R = 0x01
    ALL_BUTTONS = (BUTTON_L | BUTTON_R)

    _button_desc = { 
        BUTTON_L : "Left button",
        BUTTON_R : "Right button",
        ALL_BUTTONS : "Both buttons"
    } 

    def __init__(self):
        DefaultDelegate.__init__(self)
        self.lastVal = 0

    def handleNotification(self, hnd, data):
        # NB: only one source of notifications at present
        # so we can ignore 'hnd'.
        val = struct.unpack("B", data)[0]
        down = (val & ~self.lastVal) & self.ALL_BUTTONS
        if down != 0:
            self.onButtonDown(down)
        up = (~val & self.lastVal) & self.ALL_BUTTONS
        if up != 0:
            self.onButtonUp(up)
        self.lastVal = val

    def onButtonUp(self, but):
        print ( "** " + self._button_desc[but] + " UP")

    def onButtonDown(self, but):
        print ( "** " + self._button_desc[but] + " DOWN")



#===================================== Sensortag data reading  ====================================================


def read_sensor():
	
	global timestr1
	global count
	global folder
	global thing_name
	global arg
	global tag
	global date
	timestr1 =  time.strftime("%d.%m.%Y_%H:%M")
	os.system('rm -r /home/blt0009/AWS_IoT/sensor/*.csv')
	
	
	folder=thing_name+'/'
	print("Folder is created in client.bucket:",folder)
	s3_client.put_object(Bucket='client.bucket',Body='',Key=folder)
	
	
	time.sleep(1.0)
	while True:
		
		#date = time.strftime("%d.%m.%Y")
		#print("Before reading sensortag data:",t1)
		mqtt_client.connect(awshost, awsport, keepalive=60)
		
		if arg.temperature or arg.all:
			T=tag.IRtemperature.read()
			#print('Temp: ', tag.IRtemperature.read())
		if arg.humidity or arg.all:
			#print("Humidity: ", tag.humidity.read())
			H=tag.humidity.read()
		if arg.barometer or arg.all:
			#print("Barometer: ", tag.barometer.read())
			B=tag.barometer.read()
		if arg.accelerometer or arg.all:
			#print("Accelerometer: ", tag.accelerometer.read())
			A=tag.accelerometer.read()
		if arg.magnetometer or arg.all:
			#print("Magnetometer: ", tag.magnetometer.read())
			M=tag.magnetometer.read()
		if arg.gyroscope or arg.all:
			#print("Gyroscope: ", tag.gyroscope.read())
			G=tag.gyroscope.read()
		if (arg.light or arg.all) and tag.lightmeter is not None:
			#print("Light: ", tag.lightmeter.read())
			L=tag.lightmeter.read()	
		
		t2= time.strftime("%d.%m.%Y_%H:%M:%S")
		#print("After reading sensortag data:",t2)
		
		temp_value=str(T[0])
		humidity_value=str(H[1])
		barometer_value=str(B[1])
		accel_value1=str(A[0])
		accel_value2=str(A[1])
		accel_value3=str(A[2])
		magneto_value=str(M[0])
		gyro_value=str(G[0])
		light_value=str(L)
		main.payload="{\"state\":{\"reported\":{\"temperature\":\"" + temp_value + "\" , \"humidity\":\"" + humidity_value + "\" , \"atm_pressure\":\"" + barometer_value + "\" , \"acc1\":\"" + accel_value1 + "\" ,\"acc2\":\"" + accel_value2 + "\",\"acc3\":\"" + accel_value3 + "\", \"magnetometer\":\"" + magneto_value + "\" , \"gyrometer\":\"" + gyro_value + "\" , \"light\":\"" + light_value + "\" }}}"
		mqtt_client.publish('$aws/things/'+thing_name+'/shadow/update',main.payload,0,True)
		print("payload data:",main.payload)
		count=count+1
		data={'sensor':arg.host,'temp':T[0],'hum':H[1],'baro':B[1],'acc1':A[0],'acc2':A[1],'acc3':A[2],'lgh':L,'timestr1':timestr1}
		file_names=write_to_file(data)
		if (count<21):
			print("********* %d record was fetched into csv files & wait for 15 seconds ***********\n" % (count))
		if(count==20):
			upload_to_s3(file_names)
		
		time.sleep(13)
		
		
	tag.disconnect()	
	del tag


#=========================== Thing Regestration in AWS IoT ==========================================


def thing_register():
	
	global thing_name
	resp_create_thing = client.create_thing(
	    thingName=thing_name,
	    attributePayload={
		'attributes': {
		    'thing': 'for_creation'
		}
	    }
	)

	resp_create_cert = client.create_keys_and_certificate(
	    setAsActive=True|False
	)
	c=resp_create_cert['certificatePem']
	#print(c)
	k=resp_create_cert['keyPair']
	#print(k['PublicKey'])
	#print(k['PrivateKey']) 


	cert = {'certficatePem': c}
	certificate_output = open("certificate.pem","w")
	certificate_output.write(c)                                    
	certificate_output.close()


	publickey={'PublicKey': k['PublicKey']}
	public_output = open("publickey.pem","w")
	public_output.write(k['PublicKey'])
	public_output.close()


	privatekey={'PrivateKey': k['PrivateKey']}
	private_output = open("privatekey.pem","w")
	private_output.write(k['PrivateKey'])
	private_output.close()

	 
	policy_name =  'thing' 
	fo = open("policy_doc.json", "r+")
	policy_doc_data = fo.read(300);
	resp_create_policy = client.create_policy(
	    policyName=policy_name,
	    policyDocument=policy_doc_data
	)

	resp_attach_princ_poli = client.attach_principal_policy(
	    policyName=policy_name,
	    principal=resp_create_cert['certificateArn']

	)


	resp_attach_thin_pri = client.attach_thing_principal(
	    thingName=thing_name,
	    principal=resp_create_cert['certificateArn']
	)
	
	print('Device is registered in AWS IoT with name:',thing_name)
	


#======================== AWS IoT Configuration ==========================================
	
thingName = thing_name
caPath = "root-CA.crt"
certPath = "certificate.pem"
keyPath = "privatekey.pem"
mqtt_client.tls_set(caPath, certfile=certPath, keyfile=keyPath, cert_reqs=ssl.CERT_REQUIRED, tls_version=ssl.PROTOCOL_TLSv1_2, ciphers=None)
mqtt_client.on_connect = on_connect
mqtt_client.on_message = on_message


#============================== Connect to Sensortag ===================================


def connect_to_sensor():
	
	global thing_name
	global arg
	global tag
	parser = argparse.ArgumentParser()
	parser.add_argument('host', action='store',help='MAC of BT device')
	parser.add_argument('-n', action='store', dest='count', default=0,type=int, help="Number of times to loop data")
	parser.add_argument('-t',action='store',type=float, default=5.0, help='time between polling')
	parser.add_argument('-T','--temperature', action="store_true",default=False)	
	parser.add_argument('-A','--accelerometer', action='store_true', default=False)
	parser.add_argument('-H','--humidity', action='store_true', default=False)
	parser.add_argument('-M','--magnetometer', action='store_true', default=False)
	parser.add_argument('-B','--barometer', action='store_true', default=False)
	parser.add_argument('-G','--gyroscope', action='store_true', default=False)
	parser.add_argument('-K','--keypress', action='store_true', default=False)
	parser.add_argument('-L','--light', action='store_true', default=False)
	parser.add_argument('--all', action='store_true', default=False)
	arg = parser.parse_args(sys.argv[1:])

	print('Connecting to ' + arg.host)
	thing_name= arg.host
	
	tag = SensorTag(arg.host)
	
    # Enabling selected sensors
	if arg.temperature or arg.all:
        	tag.IRtemperature.enable()
	if arg.humidity or arg.all:
        	tag.humidity.enable()
	if arg.barometer or arg.all:
        	tag.barometer.enable()
	if arg.accelerometer or arg.all:
        	tag.accelerometer.enable()
	if arg.magnetometer or arg.all:
		tag.magnetometer.enable()
	if arg.gyroscope or arg.all:
        	tag.gyroscope.enable()
	if arg.keypress or arg.all:
        	tag.keypress.enable()
        	tag.setDelegate(KeypressDelegate())
	if arg.light and tag.lightmeter is None:
        	print("Warning: no lightmeter on this device")
	if (arg.light or arg.all) and tag.lightmeter is not None:
        	tag.lightmeter.enable()


#===========================================================================================================================


def create_role(func_name, policies=None):
    """ Create a role with an optional inline policy """
    policydoc = {
        "Version": "2012-10-17",
        "Statement": [
            {"Effect": "Allow", "Principal": {"Service": ["lambda.amazonaws.com"]}, "Action": ["sts:AssumeRole"]},
        ]
    }
    roles = [r['RoleName'] for r in iam.list_roles()['Roles']]
    if func_name in roles:
        print ('IAM role %s exists' % (func_name))
        role = iam.get_role(RoleName=func_name)['Role']
    else:
        print ('Creating IAM role %s' % (func_name))
        role = iam.create_role(RoleName=func_name, AssumeRolePolicyDocument=json.dumps(policydoc))['Role']

    # attach managed policy
    if policies is not None:
        for p in policies:
            iam.attach_role_policy(RoleName=role['RoleName'], PolicyArn=p)
    return role




#================================ Create a Lambda rule for conference call ===========================================


def create_lamba_rule(func_name):
	
	global thing_name
	rule_name ='lambda_rule'
	topic_filter = '$aws/things/' + thing_name + '/shadow/update/accepted'
	sql_statement = 'SELECT * FROM' + "'"+ topic_filter +"'" +'WHERE state.reported.light<3'
	description= 'Make a conference call when ligh value is lessthan 3'
	fun_Arn1 = 'arn:aws:lambda:us-east-1:163461573414:function:'
	fun_Arn = fun_Arn1 + func_name
	sql_ver='2016-03-23-beta'
	response = client.create_topic_rule(
	    ruleName=rule_name,
	    topicRulePayload={
		'sql': sql_statement,
		'description': description,
		'actions': [
		    {
		       
		        'lambda': {
		            'functionArn': fun_Arn 
		        },
		   
		      
		    },
		],
		'ruleDisabled': False,
		'awsIotSqlVersion': sql_ver
	    }
	)



#====================================  Lambda function creation for making a conference call =============================



def lambda_invoke(func_name, zfile, lsize=128, timeout=10, update=False):

		
	role = create_role(func_name + '_lambda', policies=['arn:aws:iam::aws:policy/service-role/AWSLambdaKinesisExecutionRole'])
	with open(zfile, 'rb') as zipfile:
		if func_name in [f['FunctionName'] for f in l.list_functions()['Functions']]:
		    if update:
		        print ('Updating %s lambda function code' % (func_name))
		        return l.update_function_code(FunctionName=func_name, ZipFile=zipfile.read())
		    else:
		        print ('Lambda function %s exists' % (func_name))
		        for f in funcs:
		            if f['FunctionName'] == func_name:
		                lfunc = f
		else:
		    print ('Creating %s lambda function' % (func_name))
		    lfunc = l.create_function(
		        FunctionName=func_name,
		        Runtime='python2.7',
		        Role=role['Arn'],
		        Handler='lambda_function.lambda_handler',
		        Description='Test invokation of lambda function',
		        Timeout=timeout,
		        MemorySize=lsize,
		        Publish=True,
		        Code={'ZipFile': zipfile.read()},
		    )
		lfunc['Role'] = role

  	
	#create_lamba_rule(func_name)




#****************************************  Execution starts here *****************************************************


def main():
	connect_to_sensor()
	#thing_register()
	#lambda_invoke('client1', 'message_lambda_function_FILES.zip', update=True)
	read_sensor()
	

if __name__ == "__main__":
	main()






