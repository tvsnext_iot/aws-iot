import boto3
import json
import time
import os
import csv

flag=0
 
client = boto3.client('iot-data', region_name='us-east-1')


def write_to_file(data):
	
	global flag
	timestr =  time.strftime("%d/%m/%Y-%H:%M:%S")
	fi=dict()	
	di={'Temperature':data['temp'],'Humidity':data['hum'],'Barometer':data['baro'],'Accelerometer':data['acc1'],'Light':data['lgh']}
	val2=data['acc2']
	val3=data['acc3']
	c=1
	
	
	for key,value in di.items():
		#with open(os.path.join('/home/blt0009/AWS_IoT/thing',data['sensor'] + '_' + key + '_' + data['timestr1'] +  '.csv'), 'a') as csv_file:	
		with open(os.path.join('/home/blt0009/AWS_IoT/live_graph',key + '.csv'),'a') as csv_file:	
			fi['fil' + str(c)] = data['sensor'] + '_' + key + '_' + data['timestr1'] + '.csv'
			try:	
				fieldnames = ['MAC_ID','SENSOR','VALUE','TIME']
				fieldnames1 = ['MAC_ID','SENSOR','VALUE1','VALUE2','VALUE3','TIME']
				writer = csv.DictWriter(csv_file, delimiter=',', lineterminator='\n',fieldnames=fieldnames)
				writer1 = csv.DictWriter(csv_file, delimiter=',', lineterminator='\n',fieldnames=fieldnames1)
				if flag==0:
					if (key == 'Accelerometer'):
						writer1.writeheader()
					else:
						writer.writeheader()

				if (key == 'Accelerometer'):
						
						writer1.writerow({'MAC_ID':data['sensor'],'SENSOR':key,'VALUE1':value,'VALUE2':val2,'VALUE3':val3,'TIME':timestr})
				else:
						writer.writerow({'MAC_ID':data['sensor'],'SENSOR':key,'VALUE':value,'TIME':timestr})
			finally:
				csv_file.close()
		c=c+1
	flag=flag+1
	return fi
		


def main():
	os.system('rm -r /home/blt0009/AWS_IoT/live_graph/*.csv')
	while(1):
		timestr1 =  time.strftime("%d.%m.%Y_%H:%M")
		response = client.get_thing_shadow(thingName='A0:E6:F8:AE:2E:85')
		streamingBody = response["payload"]
		jsonState = json.loads(streamingBody.read())
		print "Temperature:", jsonState["state"]["reported"]["temperature"]
		print "Humidity:", jsonState["state"]["reported"]["humidity"]
		print "Barometer:", jsonState["state"]["reported"]["atm_pressure"]
		print "Velocity(from accelorometer):", jsonState["state"]["reported"]["velocity"]
		print "Accelorometer_value1:",jsonState["state"]["reported"]["acc1"]
		print "Accelorometer_value2:",jsonState["state"]["reported"]["acc2"]
		print "Accelorometer_value3:",jsonState["state"]["reported"]["acc3"]
		print "Magnetometer:", jsonState["state"]["reported"]["magnetometer"]
		print "Gyrometer:", jsonState["state"]["reported"]["gyrometer"]
		print "Light:", jsonState["state"]["reported"]["light"]
		data={'sensor':'A0:E6:F8:AE:2E:85','temp':jsonState["state"]["reported"]["temperature"],'hum':jsonState["state"]["reported"]["humidity"],'baro':jsonState["state"]["reported"]["atm_pressure"],'acc1':jsonState["state"]["reported"]["velocity"],'acc2':jsonState["state"]["reported"]["velocity"],'acc3':jsonState["state"]["reported"]["velocity"],'lgh':jsonState["state"]["reported"]["light"],'timestr1':timestr1}
		file_names=write_to_file(data)
		time.sleep(15)
		print("***************** wait for 15 seconds ************************")	




if __name__ == "__main__":
	main()
