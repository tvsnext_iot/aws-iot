from __future__ import print_function
import matplotlib
matplotlib.use('GTKAgg')
import matplotlib.animation as animation
import matplotlib.dates as mdates
import matplotlib.image as mpimg
import matplotlib.pyplot as plt
from matplotlib import pyplot as plt1
from datetime import datetime
import numpy as np
import codecs
import time
import csv
import glob
import os
import re
import gtk



def clicked(button):
    for files in glob.glob("*.png"):
	pass
    sorted(files, reverse=True)
    os.system('eog '+files)

def update(event):
    if event.xdata is None:
        label.set_markup('Drag mouse over axes for position')
    else:
        label.set_markup('<span color="#ef0000">x,y=(%f, %f)</span>' % (event.xdata, event.ydata))
	

def enter_axes(event):
    event.inaxes.patch.set_facecolor('#666600')
    event.canvas.draw()

def leave_axes(event):
    event.inaxes.patch.set_facecolor('#666600')
    event.canvas.draw()

def enter_figure(event):
    event.canvas.figure.patch.set_facecolor('#FF0000')
    event.canvas.draw()

def leave_figure(event):
    event.canvas.figure.patch.set_facecolor('#C0C0C0')
    event.canvas.draw()

datefunc = lambda x: mdates.date2num(datetime.strptime(x,'%d/%m/%Y-%H:%M:%S'))

def animate(i):
	for files in glob.glob("*.csv"):
		if re.search('Accelerometer',files):
			data=np.genfromtxt(files, delimiter=',',skiprows=1,dtype="object,object,float,float,float,object",names=["id","sensor","value1","value2","value3","date"],unpack=True,converters={5:datefunc})
			ax1.clear()
			ax1.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M:%S'))
			for label in ax1.xaxis.get_ticklabels(): # rotate the value angle in x-axis
				label.set_rotation(45)
			#plt.clf()
			ax1.set_title('Accelerometer',fontsize=14, fontweight='bold',color="#000000")
			ax1.set_ylabel('Acceleration (Gs)',fontsize=14, fontweight='bold',color="#000000")
			ax1.set_xlabel('Time(Hr:min:sec)',fontsize=14, fontweight='bold',color="#000000")
			ax1.xaxis.grid(True,color='k')
			ax1.yaxis.grid(True,color='k')
			ax1.tick_params(color="#000000", labelcolor="#000000")
			ax1.text(0.95, 0.01,time.strftime("%Y/%m/%d-%H:%M:%S"),verticalalignment='bottom', horizontalalignment='right',transform=ax1.transAxes,color='white', fontsize=12)
			ax1.plot_date(data["date"],data["value1"],"-",label="y1",c='#CC0000')
			ax1.plot_date(data["date"],data["value2"],"-",label="y2",c='#FFFF00')
			ax1.plot_date(data["date"],data["value3"],"-",label="y3",c='#999900')

	for files in glob.glob("*.csv"):			
		if re.search('Barometer',files):
			data2=np.genfromtxt(files, delimiter=',',skiprows=1,dtype="object,object,float,object",names=["id","sensor","value","date"],unpack=True,converters={3:datefunc})
			ax2.clear()
			for label in ax2.xaxis.get_ticklabels(): # rotate the value angle in x-axis
				label.set_rotation(45)
			ax2.set_ylim(ymin=1000,ymax=1050)
			ax2.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M:%S'))
			ax2.set_title('Barometer',fontsize=14, fontweight='bold',color="#000000")
			ax2.set_ylabel('Barometer (mbar)',fontsize=14, fontweight='bold',color="#000000")
			ax2.xaxis.grid(True,color='k')
			ax2.yaxis.grid(True,color='k')
			ax2.text(0.95, 0.01,time.strftime("%Y/%m/%d-%H:%M:%S"),verticalalignment='bottom',horizontalalignment='right',transform=ax2.transAxes,color='white', fontsize=12)
			ax2.tick_params(color="#000000", labelcolor="#000000")
			ax2.set_xlabel('Time(Hr:min:sec)',fontsize=14, fontweight='bold',color="#000000")
			ax2.plot_date(data2["date"],data2["value"],"-",c='#6FFF00')

	for files in glob.glob("*.csv"):				
		if re.search('Humidity',files):
			data3=np.genfromtxt(files, delimiter=',',skiprows=1,dtype="object,object,float,object",names=["id","sensor","value","date"],unpack=True,converters={3:datefunc})
			ax3.clear()
			for label in ax3.xaxis.get_ticklabels(): # rotate the value angle in x-axis
				label.set_rotation(45)
			ax3.set_ylim(ymin=20,ymax=40)
			ax3.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M:%S'))
			ax3.set_title('Humidity',fontsize=14, fontweight='bold',color="#000000")
			ax3.set_ylabel('Humidity (%)',fontsize=14, fontweight='bold',color="#000000")
			ax3.xaxis.grid(True,color='k')
			ax3.yaxis.grid(True,color='k')
			ax3.text(0.95, 0.01,time.strftime("%Y/%m/%d-%H:%M:%S"),verticalalignment='bottom', horizontalalignment='right',transform=ax3.transAxes,color='white', fontsize=12)
			ax3.tick_params(color="#000000", labelcolor="#000000")
			ax3.set_xlabel('Time(Hr:min:sec)',fontsize=14, fontweight='bold',color="#000000")
			ax3.plot_date(data3["date"],data3["value"],"-",c='#6FFF00')

	for files in glob.glob("*.csv"):	
		if re.search('Light',files):
			data4=np.genfromtxt(files, delimiter=',',skiprows=1,dtype="object,object,float,object",names=["id","sensor","value","date"],unpack=True,converters={3:datefunc})
			ax4.clear()
			for label in ax4.xaxis.get_ticklabels(): # rotate the value angle in x-axis
				label.set_rotation(45)
			ax4.set_ylim(ymin=0,ymax=180)
			ax4.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M:%S'))
			ax4.set_title('Light',fontsize=14, fontweight='bold',color="#000000")
			ax4.set_ylabel(' light intensity (Lux)',fontsize=14, fontweight='bold',color="#000000")
			ax4.xaxis.grid(True,color='k')
			ax4.yaxis.grid(True,color='k')
			ax4.text(0.95, 0.01,time.strftime("%Y/%m/%d-%H:%M:%S"),verticalalignment='bottom', horizontalalignment='right',transform=ax4.transAxes,color='white', fontsize=12)
			ax4.set_xlabel('Time(Hr:min:sec)',fontsize=14, fontweight='bold',color="#000000")
			ax4.tick_params(color="#000000", labelcolor="#000000")
			ax4.plot_date(data4["date"],data4["value"],"-",c='#6FFF00')

	for files in glob.glob("*.csv"):	
		if re.search('Temperature',files):
			data5=np.genfromtxt(files, delimiter=',',skiprows=1,dtype="object,object,float,object",names=["id","sensor","value","date"],unpack=True,converters={3:datefunc})
			ax5.clear()
			for label in ax5.xaxis.get_ticklabels(): # rotate the value angle in x-axis
				label.set_rotation(45)
			ax5.set_ylim(ymin=15,ymax=40)
			ax5.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M:%S'))
			ax5.set_title('Temperature',fontsize=14, fontweight='bold',color="#000000")
			ax5.set_ylabel('Temperature (Celsius)',fontsize=14, fontweight='bold',color="#000000")
			ax5.xaxis.grid(True,color='k')
			ax5.yaxis.grid(True,color='k')
			ax5.text(0.95, 0.01,time.strftime("%Y/%m/%d-%H:%M:%S"),verticalalignment='bottom', horizontalalignment='right',transform=ax5.transAxes,color='white', fontsize=12)
			ax5.set_xlabel('Time(Hr:min:sec)',fontsize=14, fontweight='bold',color="#000000")
			ax5.tick_params(color="#000000", labelcolor="#000000")
			ax5.plot_date(data5["date"],data5["value"],"-",c='#6FFF00')
			if(i % 30 == 0):
			#print ("in if exe:",i)
				my_file = time.strftime("%H:%M")+".png"
			#my_floder = time.strftime("%d:%m:%Y")
			#os.system("mkdir "+my_floder)
			#os.system('cd /home/blt0005-/Downloads/csv_ creat/sensor2/'+my_floder)
				plt.savefig(my_file,dpi=100)
			#os.system('cd ..')




fig=plt.figure(facecolor="#C0C0C0")
ax1=fig.add_subplot(2,3,1, axisbg='#666666')
ax2=fig.add_subplot(2,3,2, axisbg='#666666')
ax3=fig.add_subplot(2,3,3, axisbg='#666666')
ax4=fig.add_subplot(2,3,4, axisbg='#666666')
ax5=fig.add_subplot(2,3,6, axisbg='#666666')


fig.canvas.mpl_connect('figure_enter_event', enter_figure)
fig.canvas.mpl_connect('figure_leave_event', leave_figure)
fig.canvas.mpl_connect('axes_enter_event', enter_axes)
fig.canvas.mpl_connect('axes_leave_event', leave_axes)

manager = plt.get_current_fig_manager()
# you can also access the window or vbox attributes this way
toolbar = manager.toolbar

# now let's add a button to the toolbar

next = 8  # where to insert this in the mpl toolbar
button = gtk.Button('OLD DATA')
button.show()

toolitem = gtk.ToolItem()
toolitem.show()
#toolitem.set_tooltip(toolbar.tooltips,'Click me for olddata show')
button.connect('clicked', clicked)


toolitem.add(button)
toolbar.insert(toolitem, next)
next += 1

# now let's add a widget to the vbox
label = gtk.Label()
label.set_markup('Drag mouse over axes for position')
label.show()
vbox = manager.vbox
vbox.pack_start(label, False, False)
vbox.reorder_child(manager.toolbar, -1)

plt.connect('motion_notify_event', update)

ani = animation.FuncAnimation(fig, animate, interval=1000)
fig.suptitle('SENSORTAG DATA IN GRAPH', fontsize=14, fontweight='bold',color="#000000")
plt.subplots_adjust(left=0.09, bottom=0.20, right=0.94, top=0.90, wspace=0.2, hspace=0.50)
fig = plt.gcf()
fig.set_size_inches(18.5, 10.5,forward=True)
plt.set_cmap('jet')
plt.legend()
plt.show()
