#!/usr/bin/env python

import re
import time
import json
import boto3


client = boto3.client('iot')

rule_name = raw_input("Enter rule name: ")
sql_statement = raw_input("Enter sql statement: ")
description= raw_input("Enter description for rule: ")

#fo = open(raw_input("Enter fun_arn file name: "), "r+")
#fun_Arn_file_data = fo.read(300);
#fun_Arn = fun_Arn_file_data

fun_Arn1 = 'arn:aws:lambda:us-east-1:163461573414:function:'
fun_Arn2 = raw_input("Enter function name for this rule: ")
fun_Arn = fun_Arn1 + fun_Arn2

print fun_Arn


sql_ver='2016-03-23-beta'

response = client.create_topic_rule(
    ruleName=rule_name,
    topicRulePayload={
        'sql': sql_statement,
        'description': description,
        'actions': [
            {
               
                'lambda': {
                    'functionArn': fun_Arn 
                },
           
              
            },
        ],
        'ruleDisabled': False,
        'awsIotSqlVersion': sql_ver
    }
)
