import paho.mqtt.client as mqtt
import os
import re
import json
import socket
import ssl
from time import sleep
import time
import struct
import math
import sys
import boto3
import boto

client = boto3.client('iot')

def on_connect(client, userdata, flags, rc):
	global thing_name
	if rc==0:
	 	mqtt_client.subscribe('$aws/things/peopleCount/shadow/update/delta',qos=0)
	

def on_message(client, userdata, msg):
    print("Message received : "+msg.topic+" | Qos: "+str(msg.qos)+" |   Date Received: "+str(msg.payload))



awshost = "AE2J99KNLEONC.iot.us-east-1.amazonaws.com"
awsport = 8883
clientId = "peopleCount"
thingName = "peopleCount"
caPath = "root-CA.crt"
certPath = "certificate.pem"
keyPath = "privatekey.pem"

mqtt_client = mqtt.Client(client_id='peopleCount',clean_session=True)
mqtt_client.on_connect = on_connect
mqtt_client.on_message = on_message

mqtt_client.tls_set(caPath, certfile=certPath, keyfile=keyPath, cert_reqs=ssl.CERT_REQUIRED, tls_version=ssl.PROTOCOL_TLSv1_2, ciphers=None)


#main.payload="{\"state\":{\"reported\":{\"temperature\":\"" + temp_value + "\" , \"humidity\":\"" + humidity_value + "\" , \"atm_pressure\":\"" + barometer_value + "\" , \"velocity\":\"" + accel_value + "\" , \"magnetometer\":\"" + magneto_value + "\" , \"gyrometer\":\"" + gyro_value + "\" , \"light\":\"" + light_value + "\" }}}"

count=1

while(1):
	mqtt_client.connect(awshost, awsport, keepalive=60)
	c=str(count)
	print('PeopleCount:',c)
	payload= "{\"state\":{\"reported\":{\"memberscount_inside\":\"" + c + "\" ,\"membercount_outside\":\"" + c + "\"}}}"
	mqtt_client.publish('$aws/things/peopleCount/shadow/update',payload,0,True)
	count=count+1
	

