#!/usr/bin/env python

import time
import json
import boto3
import re


#**************************************************************************************************************************


iam = boto3.client('iam')
l = boto3.client('lambda')


def create_role(func_name, policies=None):
    """ Create a role with an optional inline policy """
    policydoc = {
        "Version": "2012-10-17",
        "Statement": [
            {"Effect": "Allow", "Principal": {"Service": ["lambda.amazonaws.com"]}, "Action": ["sts:AssumeRole"]},
        ]
    }
    roles = [r['RoleName'] for r in iam.list_roles()['Roles']]
    if func_name in roles:
        print 'IAM role %s exists' % (func_name)
        role = iam.get_role(RoleName=func_name)['Role']
    else:
        print 'Creating IAM role %s' % (func_name)
        role = iam.create_role(RoleName=func_name, AssumeRolePolicyDocument=json.dumps(policydoc))['Role']

    # attach managed policy
    if policies is not None:
        for p in policies:
            iam.attach_role_policy(RoleName=role['RoleName'], PolicyArn=p)
    return role


def create_function(func_name, zfile, lsize=128, timeout=10, update=False):
    """ Create, or update if exists, lambda function """
    role = create_role(func_name + '_lambda', policies=['arn:aws:iam::aws:policy/service-role/AWSLambdaKinesisExecutionRole'])
    with open(zfile, 'rb') as zipfile:
        if func_name in [f['FunctionName'] for f in l.list_functions()['Functions']]:
            if update:
                print 'Updating %s lambda function code' % (func_name)
                return l.update_function_code(FunctionName=func_name, ZipFile=zipfile.read())
            else:
                print 'Lambda function %s exists' % (func_name)
                for f in funcs:
                    if f['FunctionName'] == func_name:
                        lfunc = f
        else:
            print 'Creating %s lambda function' % (func_name)
            lfunc = l.create_function(
                FunctionName=func_name,
                Runtime='python2.7',
                Role=role['Arn'],
                Handler='lambda_function.lambda_handler',
                Description='Test invokation of lambda function',
                Timeout=timeout,
                MemorySize=lsize,
                Publish=True,
                Code={'ZipFile': zipfile.read()},
            )
        lfunc['Role'] = role

#        this is policy ARN
#        print(Arn)
#        print (type(role['Arn']))
#        text_file = open("functionARN.txt", "w")
#        text_file.write(role['Arn'])
#        text_file.close()  
#        print (role['Arn'])
#        return lfunc



func_name=raw_input("Enter Lambda function name want to create: ")
zip_file=raw_input("Enter zip file to be uploaded: ")
# Create a lambda function
lfunc = create_function(func_name, zip_file, update=True)



#*******************************************************************************************************************


#!/usr/bin/env python


client = boto3.client('iot')

rule_name = raw_input("Enter rule name: ")
sql_statement = raw_input("Enter sql statement: ")
description= raw_input("Enter description for rule: ")

#fo = open(raw_input("Enter fun_arn file name: "), "r+")
#fun_Arn_file_data = fo.read(300);
#fun_Arn = fun_Arn_file_data

fun_Arn1 = 'arn:aws:lambda:us-east-1:163461573414:function:'
fun_Arn = fun_Arn1 + func_name
#print fun_Arn


sql_ver='2016-03-23-beta'

response = client.create_topic_rule(
    ruleName=rule_name,
    topicRulePayload={
        'sql': sql_statement,
        'description': description,
        'actions': [
            {
               
                'lambda': {
                    'functionArn': fun_Arn 
                },
           
              
            },
        ],
        'ruleDisabled': False,
        'awsIotSqlVersion': sql_ver
    }
)





