#!/usr/bin/env python

import re
import time
import json
import boto3


client = boto3.client('iot')

thing_name = raw_input("Enter thingname: ")
resp_create_thing = client.create_thing(
    thingName=thing_name,
    attributePayload={
        'attributes': {
            'thing': 'for_creation'
        }
    }
)



resp_create_cert = client.create_keys_and_certificate(
    setAsActive=True|False
)



c=resp_create_cert['certificatePem']
#print(c)
k=resp_create_cert['keyPair']
#print(k['PublicKey'])
#print(k['PrivateKey']) 


cert = {'certficatePem': c}
certificate_output = open("certificate.pem","w")
certificate_output.write(c)                                    
certificate_output.close()


publickey={'PublicKey': k['PublicKey']}
public_output = open("publickey.pem","w")
public_output.write(k['PublicKey'])
public_output.close()


privatekey={'PrivateKey': k['PrivateKey']}
private_output = open("privatekey.pem","w")
private_output.write(k['PrivateKey'])
private_output.close()

 

policy_name = raw_input("Enter policyname: ") 
fo = open("policy_doc.json", "r+")
policy_doc_data = fo.read(300);
resp_create_policy = client.create_policy(
    policyName=policy_name,
    policyDocument=policy_doc_data
)

resp_attach_princ_poli = client.attach_principal_policy(
    policyName=policy_name,
    principal=resp_create_cert['certificateArn']

)


resp_attach_thin_pri = client.attach_thing_principal(
    thingName=thing_name,
    principal=resp_create_cert['certificateArn']
)
